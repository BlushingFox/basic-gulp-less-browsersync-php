const gulp = require('gulp'),
  del = require('del'),
  sourcemaps = require('gulp-sourcemaps'),
  plumber = require('gulp-plumber'),
  less = require('gulp-less'),
  autoprefixer = require('gulp-autoprefixer'),
  cssnano = require('gulp-cssnano'),
  babel = require('gulp-babel'),
  webpack = require('webpack-stream'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat'),

  browserSync = require('browser-sync').create(),

  src_folder = './src/',
  dist_folder = './src/dist/'

gulp.task('clear', () => del([dist_folder]));

gulp.task('less', () => {
  return gulp.src([
    src_folder + 'less/main.less'
  ])
    .pipe(sourcemaps.init())
    .pipe(plumber())
    .pipe(less())
    .pipe(autoprefixer({
      browsers: ['last 3 versions', '> 0.5%']
    }))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'css'))
    .pipe(browserSync.stream());
});

gulp.task('js', () => {
  return gulp.src([src_folder + 'js/**/*.js'])
    .pipe(plumber())
    .pipe(webpack({
      mode: 'production'
    }))
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(concat('all.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'js'))
    .pipe(browserSync.stream());
});

gulp.task('build', gulp.series('clear', 'less', 'js'));

gulp.task('dev', gulp.series('less', 'js'));

gulp.task('serve', () => {
  return browserSync.init({
    // xampp
    proxy: "localhost/PROJECT_FOLDER",
    port: 80
  });
});

gulp.task('watch', () => {
  const watch = [
    './index.php',
    src_folder + 'php/*.php',
    src_folder + 'less/**/*.less',
    src_folder + 'js/**/*.js'
  ];

  gulp.watch(watch, gulp.series('dev')).on('change', browserSync.reload);
});

gulp.task('default', gulp.series('build', gulp.parallel('serve', 'watch')));